#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Load main components
cascade_file = ["xml/haarcascade_frontalface_alt2.xml",
                "xml/haarcascade_fullbody.xml"]

# Set color ranges (lower, upper)
color_array_red = [[120, 0, 0], [255, 38, 38]]
color_array_blue = [[0, 0, 80], [255, 50, 255]]
color_array_yellow = [[150, 100, 0], [255, 255, 100]]
color_array_green = [[0, 0, 0], [255, 255, 255]]


class TestCamera:
    @staticmethod
    def openCamera(cv2, camera):
        while True:
            # Read a frame and save it
            ret, video = camera.read()
            # Flip the actual image
            img = cv2.flip(video, 1)
            # Show the image
            cv2.imshow("original", img)
            # Exit with 'q' or 'Esc'
            key = cv2.waitKey(20)
            if key in [27]:
                break
        cv2.destroyAllWindows()
        pass

    @staticmethod
    def processingBody(cv2, camera, detection):
        if detection is "body_face":
            cascade = cv2.CascadeClassifier(cascade_file[0])
        elif detection is "body_full":
            cascade = cv2.CascadeClassifier(cascade_file[1])
        while True:
            # Read a frame and save it
            ret, video = camera.read()
            # Flip the actual image
            img = cv2.flip(video, 1)
            # Convert color image to black and white
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            # Search coordinates of faces and save position
            faces = cascade.detectMultiScale(gray, 1.5, 5)
            # Draw a rectangle on the coordinates of face
            for (x, y, w, h) in faces:
                cv2.rectangle(img, (x, y), (x + w, y + h), (255, 255, 255), 1)
                cv2.putText(img, "Object detected", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255))
                print("x=", x, ", y=", y)
            # Show the image
            cv2.imshow("original", img)
            # Exit with 'q' or 'Esc'
            key = cv2.waitKey(20)
            if key in [27]:
                break
        cv2.destroyAllWindows()
        pass

    @staticmethod
    def processingColor(cv2, np, camera, color):
        if color is "color_red":
            color_lower = np.array(color_array_red[0])
            color_upper = np.array(color_array_red[1])
        elif color is "color_blue":
            color_lower = np.array(color_array_blue[0])
            color_upper = np.array(color_array_blue[1])
        elif color is "color_yellow":
            color_lower = np.array(color_array_yellow[0])
            color_upper = np.array(color_array_yellow[1])
        elif color is "color_green":
            color_lower = np.array(color_array_blue[0])
            color_upper = np.array(color_array_blue[1])
        while True:
            # Read a frame and save it
            ret, video = camera.read()
            # Flip the actual image
            img = cv2.flip(video, 1)
            # Convert color image to hsv color
            color = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            # Process colors in video
            mask = cv2.inRange(color, color_lower, color_upper)
            # Set the color
            res = cv2.bitwise_and(img, img, mask=mask)
            # Show the image
            cv2.imshow("original", img)
            cv2.imshow("color", res)
            # Exit with 'q' or 'Esc'
            key = cv2.waitKey(20)
            if key in [27]:
                break
        cv2.destroyAllWindows()
        pass
