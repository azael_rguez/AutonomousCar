from tkinter import *
import cv2
from PIL import Image, ImageTk

face_cascade = cv2.CascadeClassifier('../haarcascade_frontalface_alt2.xml')

width, height = 800, 600
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

root = Tk()
root.bind('<Escape>', lambda e: root.quit())
lmain = Label(root)
lmain.pack()

def show_frame():
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    faces = face_cascade.detectMultiScale(cv2image, 1.3, 5)
    for (x,y,w,h) in faces:
            cv2.rectangle(cv2image, (x,y), (x+w,y+h), (255,255,255), 2)
            print("x=", x,", y=", y)
            IMAGEN.place(x=x, y=y)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(10, show_frame)

show_frame()
root.mainloop()
