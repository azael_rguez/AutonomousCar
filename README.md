# AutonomousCar

Project to understand how an autonomous car works in a low scale using open-source technologies,
like Python and OpenCV integrated into a Raspberry Pi 3.

## Getting Started

### Main dependencies

* Python: 3.6
* OpenCV (opencv-python): 3.4
* Numpy: 1.14
* Matplotlib: 2.1
* Json: 2.0
* *Scikit learn*

### Installing dependencies

Main programming language
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python3.6
sudo apt-get install python3-pip
```
Mathematics and Artificial vision libraries
```
pip3 install numpy matplotlib opecv-python
```
GUI library
```
sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-3.0
```

### Running the tests

Running the code
```
python3 main.py
```

## Authors

### Original Author and Development Lead

**Azael Rodríguez** 
* [Twitter](https://twitter.com/azael_rguez) 
* [Gitlab](https://gitlab.com/azael_rguez)

## Licence

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.

Documentation is licensed under [CC BY-NC-ND 4.0](BY-NC-ND.md).
