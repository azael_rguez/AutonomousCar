# !/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
from PIL import Image
from cam import TestCamera

__title__ = "AutonomousCar"
__author__ = "Álvaro Azael Rodríguez Rodríguez"
__email__ = "azael.rguez69@gmail.com"
__version__ = "1.0.0"
__license__ = "Copyright (C) 2018, Álvaro Azael Rodríguez Rodríguez."

if __name__ == "__main__":
    # initialize the camera
    camera = cv2.VideoCapture(0)
    if camera.isOpened():
        # Set resolution (1366, 768), (1024, 768), (800, 600)
        camera.set(3, 800)
        camera.set(4, 600)

        # Main menu
        while True:
            opc = input("Options:\n\t0. Exit\n\t1. Open camera\n\t2. Face detection\n\t3. Body detection\n\t"+
                        "4. Red color detection\n\t5. Blue color detection\n\t6. Yellow color detection\n\t"+
                        "7. Green color detection\n\t8. Distance\n\t9. Detect text\n")
            if opc is "0":
                exit()
            elif opc is "1":
                TestCamera.openCamera(cv2, camera)
            elif opc is "2":
                TestCamera.processingBody(cv2, camera, "body_face")
            elif opc is "3":
                TestCamera.processingBody(cv2, camera, "body_full")
            elif opc is "4":
                TestCamera.processingColor(cv2, np, camera, "color_red")
            elif opc is "5":
                TestCamera.processingColor(cv2, np, camera, "color_blue")
            elif opc is "6":
                TestCamera.processingColor(cv2, np, camera, "color_yellow")
            elif opc is "7":
                TestCamera.processingColor(cv2, np, camera, "color_green")
            elif opc is "8":
                print("")
            elif opc is "9":
                print("")
            else:
                print("Invalid option")

        # Close video capture
        camera.release()
    else:
        print("Error! Can't open the camera.")
