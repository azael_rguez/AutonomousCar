from tkinter import *
from tkinter import messagebox
from tkinter import ttk

class Application:

    def __init__(self, _Image, _ImageTk, _info):
        global root
        global Image, ImageTk
        global title, version, license
        
        root = Tk()
        Image = _Image
        ImageTk = _ImageTk
        title = _info["title"]
        version = _info["version"]
        license = _info["license"]

        self.Components()
        root.mainloop()
        pass


    def Components(self):
        # Screen
        width = 800
        height = 480
        x = (root.winfo_screenwidth() - width)/2
        y = (root.winfo_screenheight() - height)/2

        # Colors
        global BLACK_COLOR, WHITE_COLOR, GRAY_COLOR, BLUE_COLOR
        BLACK_COLOR = "#000"
        WHITE_COLOR = "#FFF"
        GRAY_COLOR = "#242424"
        BLUE_COLOR = "#007acc"

        # Main configuration
        root.title(title + " - v." + version)
        root.config(bg=GRAY_COLOR)
        root.minsize(width, height)
        root.maxsize(width, height)
        root.geometry('%dx%d+%d+%d' % (width, height, x, y))

        # Fonts
        SMA_FONT = ("Arial", 11)
        MED_FONT = ("Arial", 16)
        LAG_FONT = ("Arial", 20)
        EX_FONT = ("Arial", 26)

        # Icons
        global icoCubSpl, icoBack, icoCub, icoTac, icoVid, icoMap, icoCha, icoInf, icoCog, icoExt
        icoCubSpl = ImageTk.PhotoImage(Image.open("img/cube-splash.png"))
        icoBack = ImageTk.PhotoImage(Image.open("img/back.png"))
        icoCub = ImageTk.PhotoImage(Image.open("img/cube.png"))
        icoTac = ImageTk.PhotoImage(Image.open("img/tachometer.png"))
        icoVid = ImageTk.PhotoImage(Image.open("img/video.png"))
        icoMap = ImageTk.PhotoImage(Image.open("img/map.png"))
        icoCog = ImageTk.PhotoImage(Image.open("img/cog.png"))
        icoExt = ImageTk.PhotoImage(Image.open("img/exit.png"))

        # Styles
        global styLagBtn, styLblFoo, StyLblSpl
        styLagBtn = ttk.Style().configure("Lag.TButton", font=LAG_FONT)
        styLblFoo = ttk.Style().configure("Sma.TLabel", font=EX_FONT, 
            background=BLUE_COLOR, foreground=WHITE_COLOR)
        StyLblSpl = ttk.Style().configure("Spl.TLabel", font=EX_FONT, 
            background=GRAY_COLOR, foreground=WHITE_COLOR, border=0)

        # Splash Screen
        time = 1000
        self.SplashScreen(time, width, height)
        
        # Main menu
        root.after(time, lambda: self.MainMenu(width, height))
        pass


    def SplashScreen(self, time, width, height):
        x = (root.winfo_screenwidth() - width)/2
        y = (root.winfo_screenheight() - height)/4

        btnCub = ttk.Label(root, style="Spl.TLabel", text=title)
        btnCub.config(image=icoCubSpl, compound=TOP)
        btnCub.image = icoCubSpl
        btnCub.place(x=x, y=y)

        root.after(time, btnCub.place_forget)
        pass


    def MainMenu(self, width, height):
        # Buttons
        global btnCub, btnTac, btnCam, btnMap, btnCon, btnExt, lblFoo, btnBack
        global section
        section = StringVar()

        btnCub = ttk.Button(root, style="Lag.TButton", text="Smart mode")
        btnCub.config(image=icoCub, compound=TOP)
        btnCub.image = icoCub
        
        btnTac = ttk.Button(root, style="Lag.TButton", text="Tachometer")
        btnTac.config(image=icoTac, compound=TOP)
        btnTac.image = icoTac
        
        btnCam = ttk.Button(root, style="Lag.TButton", text="Video")
        btnCam.config(image=icoVid, compound=TOP, command=lambda: self.videoFun())
        btnCam.image = icoVid
        
        btnMap = ttk.Button(root, style="Lag.TButton", text="Maps")
        btnMap.config(image=icoMap, compound=TOP)
        btnMap.image = icoMap
        
        btnCon = ttk.Button(root, style="Lag.TButton", text="Config")
        btnCon.config(image=icoCog, compound=TOP, command=lambda: self.configFun())
        btnCon.image = icoCog
        
        btnExt = ttk.Button(root, style="Lag.TButton", text="Exit")
        btnExt.config(image=icoExt, compound=TOP, command=lambda: self.exitFun())
        btnExt.image = icoExt

        lblFoo = ttk.Label(root, style="Sma.TLabel", textvariable=section)
        lblFoo.config(width=43, anchor=E)
        lblFoo.place(x=0, y=0)

        btnBack = ttk.Button(root, style="Sma.TLabel")
        btnBack.config(image=icoBack, compound=LEFT, command=lambda: self.backMenu())
        btnBack.image = icoBack

        self.backMenu()
        pass


    def showMessage(kind, message):
        if kind == "Error":
            messagebox.showerror(kind, message)
            pass
        pass


    def exitFun(self):
        result = messagebox.askquestion("Exit", "Do you want to exit?")
        if result == "yes":
            root.quit()
            pass
        pass

    def backMenu(self):
        X = 100
        Y = 90
        btnCub.place(x=X, y=Y)
        X += 200
        btnTac.place(x=X, y=Y)
        X += 200
        btnCam.place(x=X, y=Y)
        X = 100
        Y += 180
        btnMap.place(x=X, y=Y)
        X += 200
        btnCon.place(x=X, y=Y)
        X += 200
        btnExt.place(x=X, y=Y)
        btnBack.place_forget()
        section.set("Menu\t")
        pass

    def newWin(self, actual):
        btnCub.place_forget()
        btnTac.place_forget()
        btnCam.place_forget()
        btnMap.place_forget()
        btnCon.place_forget()
        btnExt.place_forget()
        btnBack.place(x=0, y=0)
        section.set(actual + "\t")
        pass

    def videoFun(self):
        self.newWin("Video")
        pass

    def configFun(self):
        self.newWin("Config")
        pass

    pass
