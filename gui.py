# !/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from PIL import Image, ImageTk
#from gui import Application

__title__ = "AutonomousCar"
__author__ = "Álvaro Azael Rodríguez Rodríguez"
__email__ = "azael.rguez69@gmail.com"
__version__ = "1.0.0"
__license__ = "Apache 2.0 License"

# http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/index.html
# https://pythonprogramming.net/tkinter-depth-tutorial-making-actual-program/

def cosa():
    messagebox.showinfo("Title", "a Tk MessageBox")

def Application(root):
    # Screen
    width = 800
    height = 480
    x = (root.winfo_screenwidth() - width)/2
    y = (root.winfo_screenheight() - height)/2
    X, Y = 12, 12

    # Colors
    BLACK_COLOR = "#000"
    WHITE_COLOR = "#FFF"
    GRAY_COLOR = "#242424"
    BLUE_COLOR = "#007acc"

    # Fonts
    SMA_FONT = ("Arial", 11)
    MED_FONT = ("Arial", 16)
    LAG_FONT = ("Arial", 20)

    # Main configuration
    root.title(__title__ + " - " + __version__)
    #root.iconbitmap("img/icon.ico")
    root.config(bg=GRAY_COLOR)
    root.minsize(width, height)
    root.maxsize(width, height)
    root.geometry('%dx%d+%d+%d' % (width, height, x, y))

    # Icons
    icoCub = ImageTk.PhotoImage(Image.open('img/cube.png'))
    icoTac = ImageTk.PhotoImage(Image.open('img/tachometer.png'))
    icoVid = ImageTk.PhotoImage(Image.open('img/video.png'))
    icoMap = ImageTk.PhotoImage(Image.open('img/map.png'))
    icoCha = ImageTk.PhotoImage(Image.open('img/chart.png'))
    icoInf = ImageTk.PhotoImage(Image.open('img/info.png'))
    icoCog = ImageTk.PhotoImage(Image.open('img/cog.png'))

    # Styles
    styLagBtn = ttk.Style().configure('Lag.TButton', font=LAG_FONT, 
        background=GRAY_COLOR, foreground=GRAY_COLOR)
    styLblFoo = ttk.Style().configure('Sma.TLabel', font=SMA_FONT, 
        background=BLUE_COLOR, foreground=WHITE_COLOR)

    # Buttons
    btnCub = ttk.Button(root, style="Lag.TButton", text="Smart mode", command=cosa)
    btnCub.config(image=icoCub, compound=TOP)
    btnCub.image = icoCub
    btnCub.grid(row=0, column=0, padx=X, pady=Y)

    btnTac = ttk.Button(root, style="Lag.TButton", text="Tachometer")
    btnTac.config(image=icoTac, compound=TOP)
    btnTac.image = icoTac
    btnTac.grid(row=0, column=1, padx=X, pady=Y)

    btnCam = ttk.Button(root, style="Lag.TButton", text="Camera")
    btnCam.config(image=icoVid, compound=TOP)
    btnCam.image = icoVid
    btnCam.grid(row=0, column=2, padx=X, pady=Y)

    btnMap = ttk.Button(root, style="Lag.TButton", text="Maps")
    btnMap.config(image=icoMap, compound=TOP)
    btnMap.image = icoMap
    btnMap.grid(row=0, column=3, padx=X, pady=Y)

    btnCha = ttk.Button(root, style="Lag.TButton", text="Charts")
    btnCha.config(image=icoCha, compound=TOP)
    btnCha.image = icoCha
    btnCha.grid(row=1, column=0, padx=X, pady=Y)

    btnInf = ttk.Button(root, style="Lag.TButton", text="Info")
    btnInf.config(image=icoInf, compound=TOP)
    btnInf.image = icoInf
    btnInf.grid(row=1, column=1, padx=X, pady=Y)

    btnCon = ttk.Button(root, style="Lag.TButton", text="Configuration")
    btnCon.config(image=icoCog, compound=TOP)
    btnCon.image = icoCog
    btnCon.grid(row=1, column=2, padx=X, pady=Y)

    # Footer
    FooText = "This project is licensed under the Apache 2.0 License" + "    "
    lblFoo = ttk.Label(root, style="Sma.TLabel", text=FooText)
    lblFoo.config(width=100, anchor=E)
    lblFoo.place(x=0, y=460)


if __name__ == "__main__":
    root = Tk()
    Application(root)
    root.mainloop()
