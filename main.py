# !/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import threading
import cv2
import numpy as np
from PIL import Image, ImageTk
from data import Information
from gui import Application

if __name__ == "__main__":

    # Load main information
    LoadInfo = Information.File(json)

    # Initialize the camera
    """camera = cv2.VideoCapture(0)
    if camera.isOpened():
        Application(Image, ImageTk, LoadInfo)
    else:
        Application.showMessage("Error", "Can't open the camera.")
    pass"""
    Application(Image, ImageTk, LoadInfo)
